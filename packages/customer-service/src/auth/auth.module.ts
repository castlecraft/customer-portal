import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenCache } from './entities/token-cache/token-cache.collection';
import { TokenCacheService } from './entities/token-cache/token-cache.service';
import { TokenGuard } from './guards/token.guard';
import { AuthSchedulers } from './schedulers';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([TokenCache])],
  providers: [TokenCacheService, TokenGuard, ...AuthSchedulers],
  exports: [TokenCacheService],
})
export class AuthModule {}
