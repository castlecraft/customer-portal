import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TYPEORM_CONNECTION } from './constants/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnquiryModule } from './enquiry/enquiry.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRoot(TYPEORM_CONNECTION),
    SystemSettingsModule,
    AuthModule,
    EnquiryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
