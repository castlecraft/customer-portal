import { SETTINGS_ALREADY_EXISTS } from './messages';
import { HttpStatus, HttpException } from '@nestjs/common';

export class SettingsAlreadyExistsException extends HttpException {
  constructor() {
    super(SETTINGS_ALREADY_EXISTS, HttpStatus.BAD_REQUEST);
  }
}
