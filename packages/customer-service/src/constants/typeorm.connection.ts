import { ConfigService } from '../config/config.service';
import { ServerSettings } from '../system-settings/entities/settings/server-settings.collection';
import { TokenCache } from '../auth/entities/token-cache/token-cache.collection';
import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import { Enquiry } from '../enquiry/entities/customer-enquiry/customer-enquiry.collection';
import { EmailTemplate } from '../enquiry/entities/email-template/email-template.collection';

const config = new ConfigService();

export const TYPEORM_CONNECTION: MongoConnectionOptions = {
  type: 'mongodb',
  host: config.get('DB_HOST'),
  database: config.get('DB_NAME'),
  username: config.get('DB_USER'),
  password: config.get('DB_PASSWORD'),
  logging: false,
  entities: [ServerSettings, TokenCache, Enquiry, EmailTemplate],
  useNewUrlParser: true,
  synchronize: true,
  useUnifiedTopology: true,
};
