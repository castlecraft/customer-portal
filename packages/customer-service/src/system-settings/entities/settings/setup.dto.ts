import { IsUrl, IsNotEmpty, IsOptional } from 'class-validator';

export class SetupDto {
  uuid?: string;

  @IsUrl()
  @IsNotEmpty()
  appURL: string;

  @IsUrl()
  authServerURL: string;

  @IsOptional()
  clientId: string;

  @IsOptional()
  clientSecret: string;

  @IsUrl({ allow_underscores: true }, { each: true })
  callbackURLs: string[];

  @IsNotEmpty()
  apiKey: string;

  @IsNotEmpty()
  apiSecret: string;

  @IsOptional()
  @IsUrl()
  frappeServerURL: string;

  @IsOptional()
  type: string;
}
