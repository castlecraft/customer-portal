import { Column, Entity, BaseEntity, ObjectIdColumn, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class ServerSettings extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  type: string; // choose from service-type.ts

  @Column()
  uuid: string;

  @Column()
  appURL: string;

  @Column()
  authServerURL: string;

  @Column()
  clientId: string;

  @Column()
  clientSecret: string;

  @Column()
  profileURL: string;

  @Column()
  tokenURL: string;

  @Column()
  introspectionURL: string;

  @Column()
  authorizationURL: string;

  @Column()
  callbackURLs: string[];

  @Column()
  revocationURL: string;

  @Column()
  clientTokenUuid: string;

  @Column()
  frappeSystemManagerUuid: string;

  @Column()
  decafServerURL: string;

  @Column()
  decafClientUuid: string;

  @Column()
  apiKey: string;

  @Column()
  apiSecret: string;

  @Column()
  frappeServerURL: string;

  constructor() {
    super();
    if (!this.uuid) {
      this.uuid = uuidv4();
    }
  }
}
