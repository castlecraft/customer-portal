import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ServerSettings } from './server-settings.collection';
import { SettingsAlreadyExistsException } from '../../../constants/exceptions';
import { MongoRepository } from 'typeorm';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(ServerSettings)
    private readonly settingsRepository: MongoRepository<ServerSettings>,
  ) {}

  async save(params) {
    const serverSettings = await this.settingsRepository.findOne({
      type: params.type,
    });
    if (serverSettings) {
      throw new SettingsAlreadyExistsException();
    }
    return await this.settingsRepository.save(params);
  }

  async findOne(params) {
    return await this.settingsRepository.findOne(params);
  }

  async updateOne(query, params) {
    return await this.settingsRepository.updateOne(query, params);
  }

  async findByType(type): Promise<ServerSettings> {
    return await this.settingsRepository.findOne({ type });
  }

  async find(): Promise<ServerSettings> {
    const settings = await this.settingsRepository.find();
    return settings.length ? settings[0] : null;
  }

  async count() {
    return this.settingsRepository.count();
  }

  async findAll() {
    return await this.settingsRepository.find();
  }
}
