import { IsNotEmpty } from 'class-validator';

export class FrappeSyncDto {
  @IsNotEmpty()
  decafClientUuid: string;

  @IsNotEmpty()
  decafServerURL: string;
}
