import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
} from '@nestjs/common';
import { SetupDto } from '../../entities/settings/setup.dto';
import { CUSTOMER_SERVICE, ADMINISTRATOR } from '../../../constants/app-string';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { FrappeSyncDto } from '../../entities/settings/frappe-sync-dto';
import { SetupService } from '../../aggregates/setup/setup.service';
import { EmailTemplateDTO } from '../../../enquiry/entities/email-template/email-template-dto';

@Controller('setup')
export class SetupController {
  constructor(private readonly setupService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() payload: SetupDto) {
    const settings = Object.assign({}, payload);
    settings.type = CUSTOMER_SERVICE;
    return await this.setupService.setup(payload);
  }

  @Post('v1/frappe_sync')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  setupFrappeSync(@Body() payload: FrappeSyncDto, @Req() req) {
    return this.setupService.setupFrappeSync(payload, req);
  }

  @Post('v1/setup_email')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async setupEmail(@Body() payload: EmailTemplateDTO, @Req() req) {
    return await this.setupService.setupEmail(payload, req);
  }
}
