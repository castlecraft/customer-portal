import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServerSettings } from './entities/settings/server-settings.collection';
import { SettingsService } from './entities/settings/server-settings.service';
import { SetupController } from './controllers/setup/setup.controller';
import { SetupService } from './aggregates/setup/setup.service';
import { EnquiryModule } from '../enquiry/enquiry.module';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([ServerSettings]), EnquiryModule],
  providers: [SettingsService, SetupService],
  exports: [SettingsService, SetupService],
  controllers: [SetupController],
})
export class SystemSettingsModule {}
