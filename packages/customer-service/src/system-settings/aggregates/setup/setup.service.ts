import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { map, switchMap } from 'rxjs/operators';
import { of, throwError, from } from 'rxjs';
import { SettingsService } from '../../entities/settings/server-settings.service';
import { ServerSettings } from '../../entities/settings/server-settings.collection';
import { SettingsAlreadyExistsException } from '../../../constants/exceptions';
import { FrappeSyncDto } from '../../entities/settings/frappe-sync-dto';
import { INVALID_REQUEST } from '../../../constants/messages';
import { EmailTemplateDTO } from '../../../enquiry/entities/email-template/email-template-dto';
import { EmailTemplateService } from '../../../enquiry/entities/email-template/email-template.service';
import { uuid } from 'uuidv4';
import { TWENTY_FOUR_HOURS_IN_MINUTES } from '../../../constants/app-string';

@Injectable()
export class SetupService {
  constructor(
    protected readonly settingsService: SettingsService,
    protected readonly emailService: EmailTemplateService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.settingsService.count()) {
      throw new SettingsAlreadyExistsException();
    }

    const settings = new ServerSettings();
    Object.assign(settings, params);
    await settings.save();

    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .pipe(map(openIdConf => openIdConf.data))
      .subscribe({
        next: async response => {
          params.authorizationURL = response.authorization_endpoint;
          params.tokenURL = response.token_endpoint;
          params.profileURL = response.userinfo_endpoint;
          params.revocationURL = response.revocation_endpoint;
          params.introspectionURL = response.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          params.communicationService = response.communicationService;
          params.expireRequestLogInMinutes = TWENTY_FOUR_HOURS_IN_MINUTES;
          Object.assign(settings, params);
          await settings.save();
        },
        error: async error => {
          await settings.remove();
        },
      });
  }

  async getInfo() {
    const info = await this.settingsService.find();
    if (info) {
      info.clientSecret = undefined;
      info._id = undefined;
      info.frappeSystemManagerUuid = undefined;
      info.apiSecret = undefined;
      info.apiKey = undefined;
    }
    return info;
  }

  setupFrappeSync(payload: FrappeSyncDto, req: any) {
    if (!req || !req.token || !req.token.sub) {
      return throwError(new BadRequestException(INVALID_REQUEST));
    }
    return from(
      this.settingsService.updateOne(
        {},
        {
          $set: {
            decafClientUuid: payload.decafClientUuid,
            decafServerURL: payload.decafServerURL,
            frappeSystemManagerUuid: req.token.sub,
          },
        },
      ),
    ).pipe(
      switchMap(success => {
        return of();
      }),
    );
  }

  async setupEmail(payload: EmailTemplateDTO, req) {
    const template = await this.emailService.findOne({ type: payload.type });
    if (template) {
      return await this.emailService.updateOne(
        { uuid: template.uuid },
        { $set: template },
      );
    }

    payload.uuid = uuid();

    return await this.emailService.create(payload);
  }
}
