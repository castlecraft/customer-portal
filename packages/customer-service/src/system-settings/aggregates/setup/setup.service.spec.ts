import { Test, TestingModule } from '@nestjs/testing';
import { SetupService } from './setup.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../entities/settings/server-settings.service';
import { EmailTemplateService } from '../../../enquiry/entities/email-template/email-template.service';

describe('SetupService', () => {
  let service: SetupService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SetupService,
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: EmailTemplateService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<SetupService>(SetupService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
