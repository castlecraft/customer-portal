import { Module, Global, HttpModule } from '@nestjs/common';
import { ConfigService } from './config.service';

@Global()
@Module({
  imports: [HttpModule],
  providers: [ConfigService],
  exports: [HttpModule, ConfigService],
})
export class ConfigModule {}
