import { Test, TestingModule } from '@nestjs/testing';
import { JobController } from './job.controller';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { JobAggregateManager } from '../../aggregates/job-aggregate-manager/job-enquiry-aggregate-manager';

describe('Job Controller', () => {
  let controller: JobController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobController],
      providers: [
        {
          provide: JobAggregateManager,
          useValue: {},
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();

    controller = module.get<JobController>(JobController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
