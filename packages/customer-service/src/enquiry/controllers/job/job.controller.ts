import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { JobAggregateManager } from '../../aggregates/job-aggregate-manager/job-enquiry-aggregate-manager';
import { JobDTO } from '../../entities/job/job-enquiry-dto';

@Controller('api/job')
export class JobController {
  constructor(private readonly jobAggregate: JobAggregateManager) {}

  @Post('v1/apply')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async createNewEnquiry(@Body() payload: JobDTO) {
    return await this.jobAggregate.applyJob(payload);
  }
}
