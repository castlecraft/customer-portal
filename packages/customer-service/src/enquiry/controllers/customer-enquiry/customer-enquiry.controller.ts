import {
  Controller,
  Post,
  Body,
  Get,
  UseGuards,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateNewCustomerEnquiryCommand } from '../../commands/create-new-customer-enquiry/create-new-customer-enquiry.command';
import { GetCustomerEnquiryQuery } from '../../queries/customer-enquiry/customer-enquiry.query';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { EnquiryDTO } from '../../entities/customer-enquiry/customer-enquiry-dto';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-string';

@Controller('api/enquiry')
export class CustomerEnquiryController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async createNewEnquiry(@Body() payload: EnquiryDTO) {
    return await this.commandBus.execute(
      new CreateNewCustomerEnquiryCommand(payload),
    );
  }

  @Get('v1/list')
  @UsePipes(ValidationPipe)
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async listEnquiry() {
    return await this.queryBus.execute(new GetCustomerEnquiryQuery());
  }
}
