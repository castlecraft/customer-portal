import { CustomerEnquiryCreatedHandler } from './customer-enquiry-created/customer-enquiry-created.handler';

export const EnquiryEventHandlers = [CustomerEnquiryCreatedHandler];
