import { Global, Module, HttpModule } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CustomerEnquiryController } from './controllers/customer-enquiry/customer-enquiry.controller';
import { EnquiryEntityProvider } from './entities';
import { CustomerManagerAggregates } from './aggregates';
import { EnquiryEventHandlers } from './events';
import { EnquiryCommandHandlers } from './commands';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Enquiry } from './entities/customer-enquiry/customer-enquiry.collection';
import { QueriesHandlers } from './queries';
import { TokenGuard } from '../auth/guards/token.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import { ClientTokenManagerService } from '../system-settings/aggregates/client-token-manager/client-token-manager.service';
import { JobController } from './controllers/job/job.controller';
import { EmailTemplate } from './entities/email-template/email-template.collection';

Global();
@Module({
  imports: [
    TypeOrmModule.forFeature([Enquiry, EmailTemplate]),
    CqrsModule,
    HttpModule,
  ],
  providers: [
    TokenGuard,
    RoleGuard,
    ...EnquiryEntityProvider,
    ...CustomerManagerAggregates,
    ...EnquiryCommandHandlers,
    ...EnquiryEventHandlers,
    ...QueriesHandlers,
    ClientTokenManagerService,
  ],
  controllers: [CustomerEnquiryController, JobController],
  exports: [...EnquiryEntityProvider],
})
export class EnquiryModule {}
