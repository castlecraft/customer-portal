import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { JobDTO } from '../../entities/job/job-enquiry-dto';
import { RecursiveSyncService } from '../recursive-sync/recursive-sync.service';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { SettingsService } from '../../../system-settings/entities/settings/server-settings.service';
import { FRAPPE_JOB_APPLICANT_API_ENDPOINT } from '../../../constants/url-endpoints';

@Injectable()
export class JobAggregateManager extends AggregateRoot {
  constructor(
    private readonly recursiveSyncService: RecursiveSyncService,
    private readonly settingsService: SettingsService,
  ) {
    super();
  }

  async applyJob(jobApplication: JobDTO) {
    jobApplication.source = 'Website Listing';
    jobApplication.doctype = 'Job Applicant';
    this.syncQueryToFrappe(jobApplication);
    return true;
  }

  syncQueryToFrappe(provider: JobDTO) {
    return from(this.settingsService.find())
      .pipe(
        switchMap(settings => {
          return this.recursiveSyncService.pushToFrappeWithApiSecret(
            settings,
            settings.frappeServerURL + FRAPPE_JOB_APPLICANT_API_ENDPOINT,
            undefined,
            provider,
          );
        }),
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }
}
