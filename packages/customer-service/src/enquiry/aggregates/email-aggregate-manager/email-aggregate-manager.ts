import { HttpService, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { SettingsService } from '../../../system-settings/entities/settings/server-settings.service';
import { ClientTokenManagerService } from '../../../system-settings/aggregates/client-token-manager/client-token-manager.service';
import { map, switchMap } from 'rxjs/operators';
import { forkJoin, from, of } from 'rxjs';
import {
  INFO_ENDPOINT,
  SEND_EMAIL_ENDPOINT,
} from '../../../constants/url-endpoints';
import { COMMUNICATION_SERVER } from '../../../system-settings/entities/settings/service-type';
import { AUTHORIZATION, BEARER } from '../../../constants/app-string';

@Injectable()
export class EmailAggregateManager extends AggregateRoot {
  constructor(
    private readonly clientTokenManager: ClientTokenManagerService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
  ) {
    super();
  }

  sendEmail(emailData: EmailInterface) {
    return forkJoin({
      token: this.clientTokenManager.getClientToken(),
      settings: from(this.settingsService.find()),
    }).pipe(
      switchMap(({ token, settings }) => {
        return this.http.get(settings.authServerURL + INFO_ENDPOINT).pipe(
          map(data => data.data),
          switchMap((data: { services: { url: string; type: string }[] }) => {
            let communicationServerURL;
            data.services.forEach(service =>
              service.type === COMMUNICATION_SERVER
                ? (communicationServerURL = service.url)
                : null,
            );

            if (!communicationServerURL) {
              return of({});
            }

            return this.http.post(
              communicationServerURL + SEND_EMAIL_ENDPOINT,
              emailData,
              {
                headers: {
                  [AUTHORIZATION]: BEARER + token.accessToken,
                },
              },
            );
          }),
        );
      }),
    );
  }
}

export interface EmailInterface {
  emailTo: string;
  subject: string;
  text: string;
  html: string;
}
