import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { Enquiry } from '../../entities/customer-enquiry/customer-enquiry.collection';
import { CustomerEnquiryCreatedEvent } from '../../events/customer-enquiry-created/customer-enquiry-created.event';
import { FrappeLead } from '../../entities/customer-enquiry/frappe-lead';
import { EnquiryDTO } from '../../entities/customer-enquiry/customer-enquiry-dto';
import { RecursiveSyncService } from '../recursive-sync/recursive-sync.service';
import { concatMap, switchMap } from 'rxjs/operators';
import { forkJoin, from, of } from 'rxjs';
import { SettingsService } from '../../../system-settings/entities/settings/server-settings.service';
import { CustomerEnquiryService } from '../../entities/customer-enquiry/customer-enquiry.service';
import { FRAPPE_LEAD_API_ENDPOINT } from '../../../constants/url-endpoints';
import { EmailTemplateService } from '../../entities/email-template/email-template.service';
import { EMAIL_TEMPLATE_TYPE } from '../../../constants/app-string';
import { EmailAggregateManager } from '../email-aggregate-manager/email-aggregate-manager';

@Injectable()
export class CustomerEnquiryAggregateManager extends AggregateRoot {
  constructor(
    private readonly recursiveSyncService: RecursiveSyncService,
    private readonly settingsService: SettingsService,
    private readonly enquiryService: CustomerEnquiryService,
    private readonly emailTemplateService: EmailTemplateService,
    private readonly emailAggregate: EmailAggregateManager,
  ) {
    super();
  }

  createNewCustomerInquiry(jobRequest: EnquiryDTO) {
    jobRequest.uuid = uuidv4();
    const provider = Object.assign(new Enquiry(), jobRequest);
    provider.isSynced = false;
    this.apply(new CustomerEnquiryCreatedEvent(provider));
    const frappeLeadMetaData = new FrappeLead();

    frappeLeadMetaData.company_name = jobRequest.organizationName;
    frappeLeadMetaData.lead_name = jobRequest.name;
    frappeLeadMetaData.email_id = jobRequest.email;
    frappeLeadMetaData.phone = jobRequest.phoneNumber;

    frappeLeadMetaData.notes = `
    <div>

    <ul><li><strong>OrganizationType</strong>: ${jobRequest.organizationType}</li>

    <li><strong> Has a team</strong> : ${jobRequest.team}</li>

    <li><strong>Query Regarding</strong> : ${jobRequest.queryType}</li>

    <li><strong>Priority of lead</strong> : ${jobRequest.priority}</li>

    <li><strong>Brief Elaboration</strong> : ${jobRequest.queryBrief} </li>

    <li><strong>Budget</strong> : ${jobRequest.budget}</li>

    </ul>
    <div>`;

    this.syncQueryToFrappe(provider, frappeLeadMetaData);
  }

  async frappeLeadCreatedResponse(
    successResponse: any,
    callbackParams: { uuid: string },
  ) {
    return await this.enquiryService.updateOne(
      { uuid: callbackParams.uuid },
      { $set: { isSynced: true } },
    );
  }

  syncQueryToFrappe(provider: Enquiry, frappeLeadMetaData: FrappeLead) {
    return from(this.settingsService.find())
      .pipe(
        switchMap(settings => {
          return this.recursiveSyncService.pushToFrappeWithApiSecret(
            settings,
            settings.frappeServerURL + FRAPPE_LEAD_API_ENDPOINT,
            undefined,
            frappeLeadMetaData,
          );
        }),
      )
      .subscribe({
        next: success => {
          this.sendDefaultEmail(
            frappeLeadMetaData,
            `${new URL(success.config.url).origin}/desk#Form/Lead/${
              success.data?.data?.name
            }`,
          );
          provider.isSynced = true;
          provider
            .save()
            .then(saved => {})
            .catch(err => {});
        },
        error: error => {
          this.sendDefaultEmail(
            frappeLeadMetaData,
            `${new URL(error.config.url).origin}/desk#List/Lead/List`,
          );
          error;
        },
      });
  }

  sendDefaultEmail(frappeLeadMetaData: FrappeLead, url: string) {
    return forkJoin({
      support: from(
        this.emailTemplateService.findOne({
          type: EMAIL_TEMPLATE_TYPE.default_support_response,
        }),
      ),
      customer: from(
        this.emailTemplateService.findOne({
          type: EMAIL_TEMPLATE_TYPE.default_customer_response,
        }),
      ),
    })
      .pipe(
        concatMap(({ support, customer }) => {
          if (support) {
            const message = `
          <a href='${url || ''}'>Lead</a>
          ${frappeLeadMetaData.notes}
          `;
            support.html ? (support.html += message) : (support.html = message);
            return this.emailAggregate
              .sendEmail(support)
              .pipe(switchMap(success => of(customer)));
          }
          return of(customer);
        }),
        switchMap(customer => {
          if (customer) {
            customer.emailTo = frappeLeadMetaData.email_id;
            return this.emailAggregate.sendEmail(customer);
          }
          return of({});
        }),
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }
}
