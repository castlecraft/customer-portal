import { CustomerEnquiryAggregateManager } from './customer-enquiry-aggregate-manager/customer-enquiry-aggregate-manager';
import { EmailAggregateManager } from './email-aggregate-manager/email-aggregate-manager';
import { JobAggregateManager } from './job-aggregate-manager/job-enquiry-aggregate-manager';
import { RecursiveSyncService } from './recursive-sync/recursive-sync.service';

export const CustomerManagerAggregates = [
  CustomerEnquiryAggregateManager,
  RecursiveSyncService,
  EmailAggregateManager,
  JobAggregateManager,
];
