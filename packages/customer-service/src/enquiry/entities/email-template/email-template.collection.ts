import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class EmailTemplate extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;
  uuid?: string;

  @Column()
  type: string;

  @Column()
  emailTo: string;

  @Column()
  subject: string;

  @Column()
  text: string;

  @Column()
  html: string;

  constructor() {
    super();
    if (!this.uuid) {
      this.uuid = uuidv4();
    }
  }
}
