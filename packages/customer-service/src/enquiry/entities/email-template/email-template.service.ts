import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EmailTemplate } from './email-template.collection';
import { MongoRepository } from 'typeorm';

@Injectable()
export class EmailTemplateService {
  constructor(
    @InjectRepository(EmailTemplate)
    private readonly emailTemplateRepository: MongoRepository<EmailTemplate>,
  ) {}

  async findAll() {
    const allSettings = await this.emailTemplateRepository.find();
    return allSettings;
  }

  async create(payload) {
    const emailTemplate = new EmailTemplate();
    Object.assign(emailTemplate, payload);
    return await this.emailTemplateRepository.insertOne(emailTemplate);
  }

  async find(): Promise<EmailTemplate> {
    const settings = await this.emailTemplateRepository.find();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.emailTemplateRepository.findOne(params);
  }

  async updateOne(query, params) {
    return await this.emailTemplateRepository.updateOne(query, params);
  }

  async count() {
    return this.emailTemplateRepository.count();
  }
}
