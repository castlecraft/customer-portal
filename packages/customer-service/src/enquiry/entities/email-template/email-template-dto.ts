import { Entity } from 'typeorm';
import { IsNotEmpty, IsString, IsEmail, IsEnum } from 'class-validator';
import { EMAIL_TEMPLATE_TYPE } from '../../../constants/app-string';

@Entity()
export class EmailTemplateDTO {
  uuid?: string;

  @IsNotEmpty()
  @IsString()
  @IsEnum(Object.keys(EMAIL_TEMPLATE_TYPE))
  type: string;

  @IsNotEmpty()
  @IsEmail()
  emailTo: string;

  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsNotEmpty()
  @IsString()
  text: string;

  @IsNotEmpty()
  @IsString()
  html: string;
}
