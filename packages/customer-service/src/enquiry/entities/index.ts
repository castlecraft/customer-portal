import { CustomerEnquiryService } from './customer-enquiry/customer-enquiry.service';
import { EmailTemplateService } from './email-template/email-template.service';

export const EnquiryEntityProvider = [
  CustomerEnquiryService,
  EmailTemplateService,
];
