import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Enquiry } from './customer-enquiry.collection';
import { MongoRepository } from 'typeorm';

@Injectable()
export class CustomerEnquiryService {
  constructor(
    @InjectRepository(Enquiry)
    private readonly enquiryRepository: MongoRepository<Enquiry>,
  ) {}

  async findAll() {
    const allSettings = await this.enquiryRepository.find();
    return allSettings;
  }

  async find(): Promise<Enquiry> {
    const settings = await this.enquiryRepository.find();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.enquiryRepository.findOne(params);
  }

  async updateOne(query, params) {
    return await this.enquiryRepository.updateOne(query, params);
  }

  async count() {
    return this.enquiryRepository.count();
  }
}
