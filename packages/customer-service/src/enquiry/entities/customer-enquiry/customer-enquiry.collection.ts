import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Enquiry extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  date: Date;

  @Column()
  createdBy: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phoneNumber: string;

  @Column()
  organizationType: string;

  @Column()
  team: string;

  @Column()
  queryType: string;

  @Column()
  priority: string;

  @Column()
  queryBrief: string;

  @Column()
  isSynced: boolean;

  @Column()
  budget: string;

  constructor() {
    super();
    if (!this.uuid) {
      this.uuid = uuidv4();
    }
  }
}
