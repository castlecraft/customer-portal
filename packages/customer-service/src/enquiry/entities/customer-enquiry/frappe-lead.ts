export class FrappeLead {
  lead_name: string;
  email_id: string;
  company_name: string;
  lead_owner: string;
  phone: string;
  notes: string;
}
