import { Entity } from 'typeorm';
import { IsString, IsNotEmpty, IsEmail } from 'class-validator';

@Entity()
export class JobDTO {
  @IsString()
  @IsNotEmpty()
  applicant_name: string;

  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email_id: string;

  @IsString()
  @IsNotEmpty()
  cover_letter: string;

  source?: string;
  doctype?: string;
}
