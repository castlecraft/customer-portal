import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class JobService {
  constructor(private readonly http: HttpClient) {}

  submitQueryForm(
    name: string,
    organizationName: string,
    email: string,
    phoneNumber: string,
    organizationType: string,
    team: string,
    queryType: string,
    priority: string,
    queryBrief: string,
    budget: string,
  ) {
    const url = '/api/';

    const clientEnquiry = {
      name,
      organizationName,
      email,
      phoneNumber,
      organizationType,
      team,
      queryType,
      priority,
      queryBrief,
      budget,
    };

    return this.http.post(url + 'enquiry/v1/create', clientEnquiry);
  }
}
