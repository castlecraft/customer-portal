import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  ORGANIZATION_TYPES,
  PRIORITY_TYPE,
  BINARY,
  QUERY_TYPE,
  CLOSE,
  BUDGET_OPTIONS,
} from '../constants/storage';
import { JobService } from './client-form.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css'],
})
export class JobComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  organizations = ORGANIZATION_TYPES;
  hasTeam = BINARY;
  priorityType = PRIORITY_TYPE;
  inquiryType = QUERY_TYPE;
  url;
  params: any = {};
  budgetOptions = BUDGET_OPTIONS;
  constructor(
    private _formBuilder: FormBuilder,
    private readonly jobService: JobService,
    private readonly snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    // console.log(this.params);
    this.params.redirect = '/';
    this.firstFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      organizationName: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      phoneNumber: ['', Validators.required],
      organizationType: ['', Validators.required],
      team: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      queryType: ['', Validators.required],
      priority: ['', Validators.required],
      queryBrief: ['', Validators.required],
      budget: ['', Validators.required],
    });

    this.url = null;
    if (
      window.location.href.includes('?') &&
      window.location.href.includes('name')
    ) {
      this.getSplitUrl();
      this.getParamsFromUrl();
    }
    if (
      window.location.href.includes('?') &&
      !window.location.href.includes('name')
    ) {
      this.getSplitUrl();
    }
  }

  getParamsFromUrl() {
    this.firstFormGroup.controls.name.setValue(
      decodeURIComponent(this.params.name).replace(/[+]/g, ' '),
    );
    this.firstFormGroup.controls.email.setValue(
      decodeURIComponent(this.params.email),
    );
    this.secondFormGroup.controls.queryBrief.setValue(
      decodeURIComponent(this.params.message).replace(/[+]/g, ' '),
    );
  }

  submitForm(stepper: MatStepper) {
    this.jobService
      .submitQueryForm(
        this.firstFormGroup.controls.name.value,
        this.firstFormGroup.controls.organizationName.value,
        this.firstFormGroup.controls.email.value,
        this.firstFormGroup.controls.phoneNumber.value,
        this.firstFormGroup.controls.organizationType.value,
        this.firstFormGroup.controls.team.value,
        this.secondFormGroup.controls.queryType.value,
        this.secondFormGroup.controls.priority.value,
        this.secondFormGroup.controls.queryBrief.value,
        this.secondFormGroup.controls.budget.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open('Successfully submitted', CLOSE, {
            duration: 3500,
          });
          // const urlArray = url.split('.');
          const hostname = location.hostname.split('.');
          const baseDomain = hostname.slice(1).join('.');
          const redirect = decodeURIComponent(
            this.params.redirect || baseDomain,
          );
          window.location.href = redirect;
        },
        error: err => {
          this.snackBar.open('Could not submit enquiry', CLOSE, {
            duration: 3500,
          });
        },
      });
  }

  getSplitUrl() {
    this.url = window.location.href.split('?')[1].split('&');
    this.url.forEach(element => {
      const data = element.split('=');
      this.params[data[0]] = data[1];
    });
  }
}
