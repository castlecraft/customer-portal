import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class JobFormService {
  constructor(private readonly http: HttpClient) {}

  submitQueryForm(
    applicant_name: string,
    cover_letter: string,
    email_id: string,
  ) {
    const url = '/api/';

    const jobApplicant = {
      applicant_name,
      cover_letter,
      email_id,
    };

    return this.http.post(url + 'job/v1/apply', jobApplicant);
  }
}
