import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CLOSE, BASE_DOMAIN } from '../constants/storage';
import { JobFormService } from './job-form.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'job-form',
  templateUrl: './job-form.component.html',
  styleUrls: ['./job-form.component.css'],
})
export class JobFormComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private readonly jobService: JobFormService,
    private readonly snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      applicant_name: ['', Validators.required],
      cover_letter: ['', Validators.required],
      email_id: ['', [Validators.email, Validators.required]],
    });

    this.secondFormGroup = this._formBuilder.group({
      library: ['', Validators.required],
      framework: ['', Validators.required],
      e2e: ['', Validators.required],
      challenge: ['', Validators.required],
    });
  }

  submitForm(stepper: MatStepper) {
    const cover_letter = `
    cover: ${this.firstFormGroup.controls.cover_letter.value}

    library: ${this.secondFormGroup.controls.library.value}

    framework: ${this.secondFormGroup.controls.framework.value}

    e2e: ${this.secondFormGroup.controls.e2e.value}

    challenge : ${this.secondFormGroup.controls.challenge.value}`;

    this.jobService
      .submitQueryForm(
        this.firstFormGroup.controls.applicant_name.value,
        cover_letter,
        this.firstFormGroup.controls.email_id.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open(
            'Yay!, we have received your application and we will get back to you soon. Sending you to our website feel free to explore about us.',
            undefined,
            {
              duration: 3500,
              verticalPosition: 'top',
            },
          );
          setTimeout(() => {
            const hostname = location.hostname.split('.');
            const baseDomain = hostname.slice(1).join('.');
            const redirect = decodeURIComponent(baseDomain);
            window.location.href = redirect || BASE_DOMAIN;
          }, 3500);
        },
        error: err => {
          this.snackBar.open(
            err?.error?.message ||
              'Could not submit enquiry please try again later',
            CLOSE,
            {
              duration: 3500,
            },
          );
        },
      });
  }
}
