import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JobFormService } from './job-form.service';

describe('JobService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: JobFormService = TestBed.get(JobFormService);
    expect(service).toBeTruthy();
  });
});
