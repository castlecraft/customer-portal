export const ORGANIZATION_TYPES = [
  'Private Limited',
  'LLP',
  'General Partnership',
  'Incorporated',
  'Startup',
  'Individual',
];
export const PRIORITY_TYPE = ['Low', 'Medium', 'High', 'Critical'];
export const BINARY = ['Yes', 'No'];
export const QUERY_TYPE = [
  'Consultancy',
  'Collaboration',
  'Discussion',
  'Software Development',
  'Technical Support',
];
export const CLOSE = 'Close';
export const BUDGET_OPTIONS = [
  'No budget limit',
  'More than 100,000 USD',
  '50,000 - 100,000 USD',
  '25,000 - 50,000 USD',
  '10,000 - 25,000 USD',
  '5,000 - 10,000 USD',
  '3,000 - 5,000 USD',
  'Freebie',
];
export const BASE_DOMAIN = 'https://castlecraft.in';
