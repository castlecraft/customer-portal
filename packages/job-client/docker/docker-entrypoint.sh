#!/bin/bash

## Thanks
# https://serverfault.com/a/919212
##

set -e

if [[ -z "$API_HOST" ]]; then
    export API_HOST='localhost'
fi

if [[ -z "$API_PORT" ]]; then
    export API_PORT='5800'
fi

if [[ -z "$NGINX_PORT" ]]; then
    export NGINX_PORT='8080'
fi

envsubst '${API_HOST} ${API_PORT} ${NGINX_PORT}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
